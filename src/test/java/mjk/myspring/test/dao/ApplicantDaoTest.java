/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package mjk.myspring.test.dao;

import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.mjk.myspring.dao.IApplicantDao;
import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;

public class ApplicantDaoTest {
	
	@Autowired
	IApplicantDao applicantDao;
	
	@Test
	public void getUserByEmail() {
		
		Applicant applicant;
		User user = new User();
		
		user.setPrimaryEmail("mjk@mail.com");
		
		applicant = applicantDao.find(user);
		
		assertNull(applicant);
	}

}