/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

//package com.mjk.myspring.validation;
//
//import java.util.Locale;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
//import com.mjk.myspring.dto.SignOnDto;
//
//public class SignOnValidator extends ValidationHelper<SignOnDto> {
//	
//	public SignOnValidator(){
//		super();
//	}
//	
//	public SignOnValidator(String locale){
//		super(locale);
//	}
//	
//	@Override
//	public boolean validate(SignOnDto signOnDto)
//	{
//	    this.validateEmail(signOnDto.getSignUpEmailId());
//	    this.validateNewPassword(signOnDto.getSignUpPassword(), signOnDto.getSignUpConfirmPassword());
//		
//		return result;
//	}
//	
//	private void validateEmail(String email)
//	{
//		if(this.isNullOrEmpty(email)){
//			result = false;
//			errorMap.put("signon.email", messageSource.getMessage("message.empty.email", null, new Locale(locale)));
//			return;
//		}
//		
//		Pattern pattern;
//		Matcher matcher;
//		final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
//		
//		pattern = Pattern.compile(EMAIL_PATTERN);
//	    matcher = pattern.matcher(email);
//	    
//	    result = matcher.matches();
//	    
//	    if(!result)
//	    {
//	    	errorMap.put("signon.email", messageSource.getMessage("message.invalid.email", null, new Locale(locale)));
//	    	return;
//	    }
//	}
//	
//	private void validateNewPassword(String password, String confirmPassword)
//	{
//		if(this.isNullOrEmpty(password)){
//			result = false;
//			errorMap.put("signon.password", messageSource.getMessage("message.empty.password", null, new Locale(locale)));
//			return;
//		}
//		
//		if(this.isNullOrEmpty(confirmPassword)){
//			result = false;
//			errorMap.put("singon.confirmpassword", messageSource.getMessage("message.empty.password", null, new Locale(locale)));
//			return;
//		}
//		
//		if(!password.equals(confirmPassword)){
//			result = false;
//			errorMap.put("signon.password", messageSource.getMessage("message.mismatch.passwords", null, new Locale(locale)));
//			return;
//		}
//		
//		this.validatePasswordStrength(password);
//	}
//	
//	private void validatePasswordStrength(String password){
//		
//		final int MIN_PASSWORD_LENGTH = 8;
//		final int MAX_PASSWORD_LENGTH = 20;
//		
//		Pattern specialCharPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
//	    Pattern upperCasePattern = Pattern.compile("[A-Z ]");
//	    Pattern lowerCasePattern = Pattern.compile("[a-z ]");
//	    Pattern numberPattern = Pattern.compile("[0-9 ]");
//		
//	    switch(this.getPasswordStrength()){
//
//	    	default:
//	    
//	    	case "STRONG":
//	    		if(!specialCharPattern.matcher(password).find()){
//	    			result = false;
//	    			errorMap.put("signon.password", messageSource.getMessage("message.special.password", null, new Locale(locale)));
//	    			return;
//	    		}
//
//	    	case "MODERATE":
//	    		if(!numberPattern.matcher(password).find()){
//	    			result = false;
//	    			errorMap.replace("signon.password", messageSource.getMessage("message.number.password", null, new Locale("en")));
//	    			return;
//	    		}
//	    		
//	    	case "POOR":
//	    		if(!upperCasePattern.matcher(password).find()){
//	    			result = false;
//	    			errorMap.put("signon.password", messageSource.getMessage("message.uppercase.password", null, new Locale(locale)));
//	    			return;
//	    		}
//	    		
//	    		if(!lowerCasePattern.matcher(password).find()){
//	    			result = false;
//	    			errorMap.replace("signon.password", messageSource.getMessage("message.lowercase.password", null, new Locale(locale)));
//	    			return;
//	    		}
//	    		
//	    		if(!this.isNumberWithinRange(password.length(), MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)){
//	    			result = false;
//	    			errorMap.put("signon.password", messageSource.getMessage("message.length.password", null, new Locale(locale)));
//	    			return;
//	    		}
//	    }
//	}
//	
//	private String getPasswordStrength(){
//		return "STRONG";
//	}
//}