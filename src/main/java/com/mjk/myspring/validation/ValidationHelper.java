/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mjk.myspring.model.Applicant;

public class ValidationHelper {

	private static Logger logger = LoggerFactory.getLogger(ValidationHelper.class);
	
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public ValidationHelper(){
		logger.trace("Entry: ValidationHelper()");
		logger.debug("result: true");
	}
	
	public static boolean isEmptyOrWhitespace(final String s) {
		return (s == null || s.trim().isEmpty());
	}
	
	public static boolean isNumberWithinRange(float number, float min, float max){
		logger.debug("Number: {} - MIN: {} - MAX: {}", number, min, max);
		logger.debug("MIN CHK: {} - MAX CHK: {}", (number >= min), (number <= max));
		logger.debug("EXPR CHK: {}", (number >= min && number <= max) ? true : false);
		return (number >= min && number <= max) ? true : false;
	}
	
	public static boolean isNumberWithinRange(float number, float min, float max, boolean includingMinMax){
		
		if(!includingMinMax){
			return (number > min && number < max) ? true : false;
		}
		else{
			return isNumberWithinRange(number, min, max);
		}
	}
	
	public static boolean isEmailValid(String email){
		
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		
		return matcher.matches();
	}
	
}