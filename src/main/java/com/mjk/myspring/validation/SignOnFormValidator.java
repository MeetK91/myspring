/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.validation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.mjk.myspring.dto.SignOnDto;
import com.mjk.myspring.dto.ValidationDto;

@Component
@Qualifier("signOnFormValidator")
public class SignOnFormValidator extends ValidationHelper implements Validator {
	
	Logger logger = LoggerFactory.getLogger(SignOnFormValidator.class);

	@Override
	public boolean supports(Class<?> clazz) {
		logger.trace("Entry/Exit: supports()");
		return SignOnDto.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		logger.trace("Entry: validate()");
		
		SignOnDto input = (SignOnDto) target;
		ValidationDto validation = input.validate();
		
		if(validation.isError()) {
			errors.rejectValue(validation.getField(), validation.getFieldError());
		}
		
	}
}