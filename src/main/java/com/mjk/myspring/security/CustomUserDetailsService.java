/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.MessageSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.impl.RoleDao;
import com.mjk.myspring.dao.impl.UserDao;
//import com.mjk.myspring.model.AccessProfile;
import com.mjk.myspring.model.Privilege;
import com.mjk.myspring.model.Role;
import com.mjk.myspring.model.User;
//import com.mjk.myspring.service.IUserService;

@Transactional
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{

	static final Logger logger = LoggerFactory.getLogger(CustomUserDetailsService.class);
	
	@Autowired
    private UserDao userDao;
  
//    @Autowired
//    private IUserService service;
  
//    @Autowired
//    private MessageSource messages;
  
    @Autowired
    private RoleDao roleDao;
 
    @Override
    public UserDetails loadUserByUsername(String email)
      throws UsernameNotFoundException {
  
        User user = userDao.findByEmail(email);
        if (user == null) {
            return new org.springframework.security.core.userdetails.User(
              " ", " ", true, true, true, true, 
              getAuthorities(Arrays.asList(
                roleDao.findByName("ROLE_APPLICANT"))));
        }
 
        return new org.springframework.security.core.userdetails.User(
          user.getPrimaryEmail(), user.getPassword(), user.getIsActivated(), true, true, 
          true, getAuthorities(user.getRoles()));
    }
 
    private List<? extends GrantedAuthority> getAuthorities(List<Role> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }
 
    private List<String> getPrivileges(List<Role> roles) {
    	  
        List<String> privileges = new ArrayList<>();
        List<Privilege> list = new ArrayList<>();
        for (Role role : roles) {
        	logger.debug(role.getName() + " - " + role.getPrivileges().size());
            list.addAll(role.getPrivileges());
        }
        for (Privilege item : list) {
            privileges.add(item.getPath());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
        	logger.debug(privilege);
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }	
}