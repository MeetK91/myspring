/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.security;

import java.math.BigInteger;
import java.security.SecureRandom;

public enum TokenGenerator {

	INSTANCE;
	
	private final SecureRandom secureTokenGenerator;
	
	private TokenGenerator(){
		secureTokenGenerator = new SecureRandom();
	}
	
	public String getNewStringToken(){
		return new BigInteger(130, secureTokenGenerator).toString(32);
	}
	
	public BigInteger getNewBigIntToken(){
		return new BigInteger(130, secureTokenGenerator);
	}
}