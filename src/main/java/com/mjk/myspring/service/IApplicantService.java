/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service;

import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;

public interface IApplicantService {

	public Applicant find(User user);
	
	public boolean exists(Applicant applicant);
	
	public void saveApplicant(Applicant applicant);
	
	public boolean validate(Applicant applicant);
	
}