/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IApplicantDao;
import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;
import com.mjk.myspring.service.IApplicantService;

@Service("applicantService")
@Transactional
public class ApplicantService implements IApplicantService {

	@Autowired
	IApplicantDao applicantDao;
	
	@Override
	public Applicant find(User user) {
		return applicantDao.find(user);
	}
	
	@Override
	public boolean exists(Applicant applicant) {
		try {
			return (applicantDao.find(applicant) == null) ? false : true;
		}
		catch(NoResultException exc) {
			System.out.println(exc.toString());
			return false;
		}
	}
	
	@Override
	public boolean validate(Applicant applicant) {
		
		boolean result;
		
		result = !exists(applicant);
		
		return result;
	}

	@Override
	public void saveApplicant(Applicant applicant) {
		applicantDao.save(applicant);
	}

}