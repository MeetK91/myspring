/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mjk.myspring.dao.IRoleDao;
import com.mjk.myspring.model.Role;
import com.mjk.myspring.service.IRoleService;

@Service
public class RoleService implements IRoleService {

	Logger logger = LoggerFactory.getLogger(RoleService.class);
	
	@Autowired
	IRoleDao roleDao;
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<Role> getRolesForNewUser(Class userType) throws IllegalArgumentException {
	
		switch(userType.getSimpleName()) {
		
			case "Applicant":
				List<Role> roles = new ArrayList<Role>();
				roles.add(roleDao.findByName("ROLE_APPLICANT"));
				return roles;
				
			default:
				throw new IllegalArgumentException("User-type '" + userType.getSimpleName() + "' is not supported.");
		}
	}

}
