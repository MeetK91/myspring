/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IMenuDao;
import com.mjk.myspring.model.Menu;
import com.mjk.myspring.model.Sidebar;
import com.mjk.myspring.service.ISidebarService;

/**
 * 
 * This class will handle manipulations and fetch requests for the Sidebar object.
 * 
 * @author Meet Kulkarni
 * @see com.mjk.myspring.model.Sidebar
 * 		com.mjk.myspring.model.Menu
 */
@Service("sidebarService")
@Transactional
public class SidebarService implements ISidebarService {

	@Autowired
	IMenuDao menuDao;
	
	/**
	 * Return a suitable Sidebar object depending on whether an active user session is available or not.
	 * @see com.mjk.myspring.service.ISidebarService#getSidebar()
	 */
	@Override
	@Bean
	@Scope("session")
	public Sidebar getSidebar(boolean isUserLoggedIn) {
		//TODO Find a way to prevent recurrent calls to db just to get the sidebar (e.g. every time a page is refreshed or opened).
		//TODO Explore using @Bean on Sidebar constructor. Worst case scenario - try saving Sidebar object in Session.  
		List<Menu> menus = menuDao.getMenus(isUserLoggedIn);
		return new Sidebar(menus);
	}
}