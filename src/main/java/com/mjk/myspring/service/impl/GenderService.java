/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IGenderDao;
import com.mjk.myspring.model.Gender;
import com.mjk.myspring.service.IGenderService;

@Service("genderService")
@Transactional
public class GenderService implements IGenderService {

	@Autowired
	IGenderDao genderDao;
	
	@Override
	public Gender find(String shortCode) {
		return genderDao.find(shortCode);
	}

}
