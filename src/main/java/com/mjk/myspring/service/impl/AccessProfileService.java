/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IAccessProfileDao;
import com.mjk.myspring.model.AccessProfile;
import com.mjk.myspring.service.IAccessProfileService;


@Service("accessProfileService")
@Transactional
public class AccessProfileService implements IAccessProfileService {
	
	@Autowired
	IAccessProfileDao dao;
	
	public AccessProfile findById(int id) {
		return dao.findById(id);
	}

	public AccessProfile findByType(String type){
		return dao.findByType(type);
	}

	public List<AccessProfile> findAll() {
		return dao.findAll();
	}
}