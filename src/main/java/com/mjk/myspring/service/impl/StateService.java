/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IStateDao;
import com.mjk.myspring.model.State;
import com.mjk.myspring.service.IStateService;

@Service("stateService")
@Transactional
public class StateService implements IStateService {

	@Autowired
	IStateDao stateDao;
	
	@Override
	public State find(String shortCode) {
		return stateDao.find(shortCode);
	}

}
