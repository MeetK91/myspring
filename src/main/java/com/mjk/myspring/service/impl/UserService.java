/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.IUserDao;
import com.mjk.myspring.dto.SignOnDto;
import com.mjk.myspring.model.AccessProfile;
import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;
import com.mjk.myspring.security.TokenGenerator;
import com.mjk.myspring.service.IAccessProfileService;
import com.mjk.myspring.service.IApplicantService;
import com.mjk.myspring.service.ICountryService;
import com.mjk.myspring.service.IGenderService;
import com.mjk.myspring.service.IRoleService;
import com.mjk.myspring.service.IStateService;
import com.mjk.myspring.service.IUserService;

@Service("userService")
@Transactional
public class UserService implements IUserService {

	@Autowired
	private IUserDao userDao;
	
	@Autowired
	ApplicationContext appCtx;
	
	@Override
	public User findById(int id) {
		return userDao.findById(id);
	}

	@Override
	public boolean isUserEmailUnique(String email){
		return (userDao.findByEmail(email) == null) ? true : false;
	}
	
	@Override
	public User findByEmail(String email) {
		User user = userDao.findByEmail(email);
		return user;
	}
	
	@Override
	public int getNewId() {
		return userDao.newId();
	}
	
	@Override
	public boolean exists(User user) {
		
		try {
			return userDao.findById(user.getId()) == null ? false : true;
		}
		catch(NoResultException exc) {
			return false;
		}
	}
	
	@Override
	public boolean validate(User user) {
		
		boolean result;
		
		result = !exists(user);
		
		return result;
	}

	public void saveUser(User user) {
		userDao.save(user);
	}

	/**
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateUser(User user) {
		
		PasswordEncoder passwordEncoder = (PasswordEncoder) appCtx.getBean("passwordEncoder");
		
		User entity = userDao.findById(user.getId());
		if(entity!=null){
			entity.setPrimaryEmail(user.getPrimaryEmail());
			if(!user.getPassword().equals(entity.getPassword())){
				entity.setPassword(passwordEncoder.encode(user.getPassword()));
			}
			entity.setPrimaryEmail(user.getPrimaryEmail());
			entity.setUserProfileMappingList(user.getUserProfileMappingList());
		}
	}
	
	public void deleteUserByEmail(String email) {
		userDao.deleteByEmail(email);
	}

	public List<User> findAllUsers() {
		return userDao.findAllUsers();
	}
	
	public User registerNewUser(SignOnDto signOnDto){

		IAccessProfileService accessProfileService = appCtx.getBean("accessProfileService", IAccessProfileService.class);
		ICountryService countryService = appCtx.getBean("countryService", ICountryService.class);
		IGenderService genderService = appCtx.getBean("genderService", IGenderService.class);
		IStateService stateService = appCtx.getBean("stateService", IStateService.class);
		PasswordEncoder passwordEncoder = appCtx.getBean("passwordEncoder", PasswordEncoder.class);
		IRoleService roleService = appCtx.getBean("roleService", IRoleService.class);
		IApplicantService applicantService = appCtx.getBean("applicantService", IApplicantService.class);
		
		if(!isUserEmailUnique(signOnDto.getSignUpEmailId())){
			return null;
		}
		
		User user = new User();
		List<AccessProfile> profiles = new ArrayList<AccessProfile>();
		AccessProfile profile = accessProfileService.findByType("APPLICANT");
		Date tmpDate = new Date();
		
		Applicant applicant = new Applicant();
		
		applicant.setEmailId(signOnDto.getSignUpEmailId());
		applicant.setApplicantNo("123");
		applicant.setFirstName(signOnDto.getFirstName());
		applicant.setLastName(signOnDto.getLastName());
		applicant.setCreatedAt(tmpDate);
		applicant.setModifiedAt(tmpDate);
		applicant.setDob(tmpDate);
		applicant.setRegistrationDate(tmpDate);
		applicant.setCreatedBy("sysproxy");
		applicant.setModifiedBy("sysproxy");
		applicant.setMobileNo("1234567890");
		applicant.setCountryId(countryService.find("IN"));
		applicant.setGender(genderService.find("F"));
		applicant.setStateId(stateService.find("GJ"));
		
		profiles.add(profile);
		user.setId(TokenGenerator.INSTANCE.getNewBigIntToken().intValue());
		user.setPrimaryEmail(signOnDto.getSignUpEmailId());
		user.setPassword(passwordEncoder.encode(signOnDto.getSignUpConfirmPassword()));
		user.setAccessProfiles(profiles);
		user.setRoles(roleService.getRolesForNewUser(Applicant.class));
		user.setCreatedAt(tmpDate);
		user.setModifiedAt(tmpDate);
		user.setCreatedBy("sysproxy");
		user.setModifiedBy("sysproxy");
		user.setIsActivated(true);
		
		if(validate(user) && applicantService.validate(applicant)) {
			this.saveUser(user);
			applicantService.saveApplicant(applicant);
			return user;
		}
		
		return null;
	}
}