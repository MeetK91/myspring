/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.service;

import java.util.List;

import com.mjk.myspring.dto.SignOnDto;
import com.mjk.myspring.model.User;


public interface IUserService {
	
	public User findById(int id);
	
	public User findByEmail(String email);
	
	public int getNewId();
	
	public boolean exists(User user);
	 
	public void saveUser(User user);
	 
	public void updateUser(User user);
	 
	public void deleteUserByEmail(String email);
	 
	public List<User> findAllUsers(); 
	 
	public boolean isUserEmailUnique(String email);
	 
	public User registerNewUser(SignOnDto signOnDto);
	
	public boolean validate(User user);

}