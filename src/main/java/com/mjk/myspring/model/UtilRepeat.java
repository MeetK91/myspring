/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_util_repeat")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UtilRepeat.findAll", query = "SELECT u FROM UtilRepeat u"),
    @NamedQuery(name = "UtilRepeat.findById", query = "SELECT u FROM UtilRepeat u WHERE u.id = :id"),
    @NamedQuery(name = "UtilRepeat.findByRepeatDays", query = "SELECT u FROM UtilRepeat u WHERE u.repeatDays = :repeatDays"),
    @NamedQuery(name = "UtilRepeat.findByRepeatLabel", query = "SELECT u FROM UtilRepeat u WHERE u.repeatLabel = :repeatLabel"),
    @NamedQuery(name = "UtilRepeat.findByCreatedAt", query = "SELECT u FROM UtilRepeat u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "UtilRepeat.findByCreatedBy", query = "SELECT u FROM UtilRepeat u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "UtilRepeat.findByModifiedAt", query = "SELECT u FROM UtilRepeat u WHERE u.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "UtilRepeat.findByModifiedBy", query = "SELECT u FROM UtilRepeat u WHERE u.modifiedBy = :modifiedBy")})
public class UtilRepeat implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "repeat_days")
    private int repeatDays;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "repeat_label")
    private String repeatLabel;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;

    public UtilRepeat() {
    }

    public UtilRepeat(Integer id) {
        this.id = id;
    }

    public UtilRepeat(Integer id, int repeatDays, String repeatLabel, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.repeatDays = repeatDays;
        this.repeatLabel = repeatLabel;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(int repeatDays) {
        this.repeatDays = repeatDays;
    }

    public String getRepeatLabel() {
        return repeatLabel;
    }

    public void setRepeatLabel(String repeatLabel) {
        this.repeatLabel = repeatLabel;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UtilRepeat)) {
            return false;
        }
        UtilRepeat other = (UtilRepeat) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.UtilRepeat[ id=" + id + " ]";
    }
    
}
