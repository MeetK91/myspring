/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_user_profile_mapping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserProfileMapping.findAll", query = "SELECT u FROM UserProfileMapping u"),
    @NamedQuery(name = "UserProfileMapping.findByUserProfileMappingId", query = "SELECT u FROM UserProfileMapping u WHERE u.userProfileMappingId = :userProfileMappingId"),
    @NamedQuery(name = "UserProfileMapping.findByCreatedAt", query = "SELECT u FROM UserProfileMapping u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "UserProfileMapping.findByCreatedBy", query = "SELECT u FROM UserProfileMapping u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "UserProfileMapping.findByModifiedAt", query = "SELECT u FROM UserProfileMapping u WHERE u.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "UserProfileMapping.findByModifiedBy", query = "SELECT u FROM UserProfileMapping u WHERE u.modifiedBy = :modifiedBy")})
public class UserProfileMapping implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_profile_mapping_id")
    private Integer userProfileMappingId;
    
	@Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    
	@Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;
    
	@JoinColumn(name = "access_profile_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private AccessProfile accessProfileId;
    
	@JoinColumn(name = "user_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private User userId;

    public UserProfileMapping() {
    }

    public UserProfileMapping(Integer userProfileMappingId) {
        this.userProfileMappingId = userProfileMappingId;
    }

    public UserProfileMapping(Integer userProfileMappingId, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.userProfileMappingId = userProfileMappingId;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getUserProfileMappingId() {
        return userProfileMappingId;
    }

    public void setUserProfileMappingId(Integer userProfileMappingId) {
        this.userProfileMappingId = userProfileMappingId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public AccessProfile getAccessProfileId() {
        return accessProfileId;
    }

    public void setAccessProfileId(AccessProfile accessProfileId) {
        this.accessProfileId = accessProfileId;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userProfileMappingId != null ? userProfileMappingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProfileMapping)) {
            return false;
        }
        UserProfileMapping other = (UserProfileMapping) object;
        if ((this.userProfileMappingId == null && other.userProfileMappingId != null) || (this.userProfileMappingId != null && !this.userProfileMappingId.equals(other.userProfileMappingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.UserProfileMapping[ userProfileMappingId=" + userProfileMappingId + " ]";
    }
    
}
