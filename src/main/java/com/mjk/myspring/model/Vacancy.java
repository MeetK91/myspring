/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_vacancy")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vacancy.findAll", query = "SELECT v FROM Vacancy v"),
    @NamedQuery(name = "Vacancy.findById", query = "SELECT v FROM Vacancy v WHERE v.id = :id"),
    @NamedQuery(name = "Vacancy.findByDesignationId", query = "SELECT v FROM Vacancy v WHERE v.designationId = :designationId"),
    @NamedQuery(name = "Vacancy.findByVacantPosts", query = "SELECT v FROM Vacancy v WHERE v.vacantPosts = :vacantPosts"),
    @NamedQuery(name = "Vacancy.findByPostingDateTime", query = "SELECT v FROM Vacancy v WHERE v.postingDateTime = :postingDateTime"),
    @NamedQuery(name = "Vacancy.findByFulfilmentDateTime", query = "SELECT v FROM Vacancy v WHERE v.fulfilmentDateTime = :fulfilmentDateTime"),
    @NamedQuery(name = "Vacancy.findByAutocloseAfterDays", query = "SELECT v FROM Vacancy v WHERE v.autocloseAfterDays = :autocloseAfterDays"),
    @NamedQuery(name = "Vacancy.findByIsAutoclosed", query = "SELECT v FROM Vacancy v WHERE v.isAutoclosed = :isAutoclosed"),
    @NamedQuery(name = "Vacancy.findByCreatedAt", query = "SELECT v FROM Vacancy v WHERE v.createdAt = :createdAt"),
    @NamedQuery(name = "Vacancy.findByCreatedBy", query = "SELECT v FROM Vacancy v WHERE v.createdBy = :createdBy"),
    @NamedQuery(name = "Vacancy.findByModifiedAt", query = "SELECT v FROM Vacancy v WHERE v.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "Vacancy.findByModifiedBy", query = "SELECT v FROM Vacancy v WHERE v.modifiedBy = :modifiedBy")})
public class Vacancy implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "designation_id")
    private int designationId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "vacant_posts")
    private int vacantPosts;
    @Basic(optional = false)
    @NotNull
    @Column(name = "posting_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date postingDateTime;
    @Column(name = "fulfilment_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fulfilmentDateTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "autoclose_after_days")
    private int autocloseAfterDays;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_autoclosed")
    private boolean isAutoclosed;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;

    public Vacancy() {
    }

    public Vacancy(Integer id) {
        this.id = id;
    }

    public Vacancy(Integer id, int designationId, int vacantPosts, Date postingDateTime, int autocloseAfterDays, boolean isAutoclosed, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.designationId = designationId;
        this.vacantPosts = vacantPosts;
        this.postingDateTime = postingDateTime;
        this.autocloseAfterDays = autocloseAfterDays;
        this.isAutoclosed = isAutoclosed;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getDesignationId() {
        return designationId;
    }

    public void setDesignationId(int designationId) {
        this.designationId = designationId;
    }

    public int getVacantPosts() {
        return vacantPosts;
    }

    public void setVacantPosts(int vacantPosts) {
        this.vacantPosts = vacantPosts;
    }

    public Date getPostingDateTime() {
        return postingDateTime;
    }

    public void setPostingDateTime(Date postingDateTime) {
        this.postingDateTime = postingDateTime;
    }

    public Date getFulfilmentDateTime() {
        return fulfilmentDateTime;
    }

    public void setFulfilmentDateTime(Date fulfilmentDateTime) {
        this.fulfilmentDateTime = fulfilmentDateTime;
    }

    public int getAutocloseAfterDays() {
        return autocloseAfterDays;
    }

    public void setAutocloseAfterDays(int autocloseAfterDays) {
        this.autocloseAfterDays = autocloseAfterDays;
    }

    public boolean getIsAutoclosed() {
        return isAutoclosed;
    }

    public void setIsAutoclosed(boolean isAutoclosed) {
        this.isAutoclosed = isAutoclosed;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vacancy)) {
            return false;
        }
        Vacancy other = (Vacancy) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.Vacancy[ id=" + id + " ]";
    }
    
}
