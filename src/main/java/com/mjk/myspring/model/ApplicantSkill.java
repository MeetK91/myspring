/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_applicant_skill")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ApplicantSkill.findAll", query = "SELECT a FROM ApplicantSkill a"),
    @NamedQuery(name = "ApplicantSkill.findById", query = "SELECT a FROM ApplicantSkill a WHERE a.id = :id"),
    @NamedQuery(name = "ApplicantSkill.findBySkillId", query = "SELECT a FROM ApplicantSkill a WHERE a.skillId = :skillId"),
    @NamedQuery(name = "ApplicantSkill.findByStrength", query = "SELECT a FROM ApplicantSkill a WHERE a.strength = :strength"),
    @NamedQuery(name = "ApplicantSkill.findByCreatedAt", query = "SELECT a FROM ApplicantSkill a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "ApplicantSkill.findByCreatedBy", query = "SELECT a FROM ApplicantSkill a WHERE a.createdBy = :createdBy"),
    @NamedQuery(name = "ApplicantSkill.findByModifiedAt", query = "SELECT a FROM ApplicantSkill a WHERE a.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "ApplicantSkill.findByModifiedBy", query = "SELECT a FROM ApplicantSkill a WHERE a.modifiedBy = :modifiedBy")})
public class ApplicantSkill implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "skill_id")
    private int skillId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "strength")
    private int strength;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;
    @JoinColumn(name = "applicant_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Applicant applicantId;
    @JoinColumn(name = "id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Skill skill;

    public ApplicantSkill() {
    }

    public ApplicantSkill(Integer id) {
        this.id = id;
    }

    public ApplicantSkill(Integer id, int skillId, int strength, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.skillId = skillId;
        this.strength = strength;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Applicant getApplicantId() {
        return applicantId;
    }

    public void setApplicantId(Applicant applicantId) {
        this.applicantId = applicantId;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ApplicantSkill)) {
            return false;
        }
        ApplicantSkill other = (ApplicantSkill) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.ApplicantSkill[ id=" + id + " ]";
    }
    
}
