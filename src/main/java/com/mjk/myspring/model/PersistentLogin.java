/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_persistent_login")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PersistentLogin.findAll", query = "SELECT p FROM PersistentLogin p"),
    @NamedQuery(name = "PersistentLogin.findByUserName", query = "SELECT p FROM PersistentLogin p WHERE p.userName = :userName"),
    @NamedQuery(name = "PersistentLogin.findBySeries", query = "SELECT p FROM PersistentLogin p WHERE p.series = :series"),
    @NamedQuery(name = "PersistentLogin.findByToken", query = "SELECT p FROM PersistentLogin p WHERE p.token = :token"),
    @NamedQuery(name = "PersistentLogin.findByLastUsed", query = "SELECT p FROM PersistentLogin p WHERE p.lastUsed = :lastUsed")})
public class PersistentLogin implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "user_name")
    private String userName;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "series")
    private String series;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 64)
    @Column(name = "token")
    private String token;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "last_used")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUsed;

    public PersistentLogin() {
    }

    public PersistentLogin(String series) {
        this.series = series;
    }

    public PersistentLogin(String series, String userName, String token, Date lastUsed) {
        this.series = series;
        this.userName = userName;
        this.token = token;
        this.lastUsed = lastUsed;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getLastUsed() {
        return lastUsed;
    }

    public void setLastUsed(Date lastUsed) {
        this.lastUsed = lastUsed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (series != null ? series.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersistentLogin)) {
            return false;
        }
        PersistentLogin other = (PersistentLogin) object;
        if ((this.series == null && other.series != null) || (this.series != null && !this.series.equals(other.series))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.PersistentLogin[ series=" + series + " ]";
    }
    
}
