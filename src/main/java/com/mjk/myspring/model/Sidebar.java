/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;


/**
 * This class is to be used for generating the Sidebar. It contains a list of Menu objects for populating the menus,
 * and a boolean to set whether the sidebar will be expanded or collapsed upon rendering.
 *  
 * @author Meet Kulkarni
 * @see com.mjk.myspring.model.Menu
 * 
 */

@Component
public class Sidebar implements Serializable {

	/**
	 * IDE-generated serialVersionUID.
	 */
	private static final long serialVersionUID = 214432233992117177L;
	
	/**
	 * Menus to be populated in the sidebar.
	 * 
	 * @see com.mjk.myspring.model.Menu
	 */
	private List<Menu> menus;
	
	/**
	 * Property to programmatically set sidebar as expanded or collapsed.
	 */
	private boolean isOpen;
	
	/**
	 * Object for the logged-in user. Contains null if no user is logged in.
	 */
	private User loggedInUser;
	
	public Sidebar() {
		this.setMenus(null);
		this.setOpen(false);
		this.setLoggedInUser(null);
	}
	
	public Sidebar(List<Menu> menus) {
		this.setMenus(menus);
		this.setOpen(false);
		this.setLoggedInUser(null);
	}
	
	public Sidebar(List<Menu> menus, boolean isOpen) {
		this.setMenus(menus);
		this.setOpen(isOpen);
		this.setLoggedInUser(null);
	}
	
	public List<Menu> getMenus() {
		return menus;
	}
	
	public void setMenus(List<Menu> menus) {
		this.menus = menus;
	}
	
	public boolean isOpen() {
		return isOpen;
	}
	
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public User getLoggedInUser() {
		return loggedInUser;
	}

	public void setLoggedInUser(User loggedInUser) {
		this.loggedInUser = loggedInUser;
	}
	
	public String toString() {
		//TODO - Modify this method to suit future needs.
		return "Sidebar - toString()";
	}
}