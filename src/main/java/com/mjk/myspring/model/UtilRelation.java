/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_util_relation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UtilRelation.findAll", query = "SELECT u FROM UtilRelation u"),
    @NamedQuery(name = "UtilRelation.findById", query = "SELECT u FROM UtilRelation u WHERE u.id = :id"),
    @NamedQuery(name = "UtilRelation.findByRelation", query = "SELECT u FROM UtilRelation u WHERE u.relation = :relation"),
    @NamedQuery(name = "UtilRelation.findByCreatedAt", query = "SELECT u FROM UtilRelation u WHERE u.createdAt = :createdAt"),
    @NamedQuery(name = "UtilRelation.findByCreatedBy", query = "SELECT u FROM UtilRelation u WHERE u.createdBy = :createdBy"),
    @NamedQuery(name = "UtilRelation.findByModifiedAt", query = "SELECT u FROM UtilRelation u WHERE u.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "UtilRelation.findByModifiedBy", query = "SELECT u FROM UtilRelation u WHERE u.modifiedBy = :modifiedBy")})
public class UtilRelation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "relation")
    private String relation;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;

    public UtilRelation() {
    }

    public UtilRelation(Integer id) {
        this.id = id;
    }

    public UtilRelation(Integer id, String relation, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.relation = relation;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UtilRelation)) {
            return false;
        }
        UtilRelation other = (UtilRelation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.UtilRelation[ id=" + id + " ]";
    }
    
}
