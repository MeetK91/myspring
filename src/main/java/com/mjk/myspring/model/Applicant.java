/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_applicant")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Applicant.findAll", query = "SELECT a FROM Applicant a"),
    @NamedQuery(name = "Applicant.findById", query = "SELECT a FROM Applicant a WHERE a.id = :id"),
    @NamedQuery(name = "Applicant.findByApplicantNo", query = "SELECT a FROM Applicant a WHERE a.applicantNo = :applicantNo"),
    @NamedQuery(name = "Applicant.findByFirstName", query = "SELECT a FROM Applicant a WHERE a.firstName = :firstName"),
    @NamedQuery(name = "Applicant.findByLastName", query = "SELECT a FROM Applicant a WHERE a.lastName = :lastName"),
    @NamedQuery(name = "Applicant.findByProfilePic", query = "SELECT a FROM Applicant a WHERE a.profilePic = :profilePic"),
    @NamedQuery(name = "Applicant.findByDob", query = "SELECT a FROM Applicant a WHERE a.dob = :dob"),
    @NamedQuery(name = "Applicant.findByRegistrationDate", query = "SELECT a FROM Applicant a WHERE a.registrationDate = :registrationDate"),
    @NamedQuery(name = "Applicant.findByMobileNo", query = "SELECT a FROM Applicant a WHERE a.mobileNo = :mobileNo"),
    @NamedQuery(name = "Applicant.findByCreatedAt", query = "SELECT a FROM Applicant a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "Applicant.findByCreatedBy", query = "SELECT a FROM Applicant a WHERE a.createdBy = :createdBy"),
    @NamedQuery(name = "Applicant.findByModifiedAt", query = "SELECT a FROM Applicant a WHERE a.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "Applicant.findByModifiedBy", query = "SELECT a FROM Applicant a WHERE a.modifiedBy = :modifiedBy")})
public class Applicant implements Serializable {
	
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "email_id")
    private String emailId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "applicant_no")
    private String applicantNo;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "first_name")
    private String firstName;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "last_name")
    private String lastName;
    
    @Size(max = 100)
    @Column(name = "profile_pic")
    private String profilePic;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "dob")
    @Temporal(TemporalType.DATE)
    private Date dob;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "registration_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date registrationDate;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "mobile_no")
    private String mobileNo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;
    
    @JoinColumn(name = "country_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Country countryId;
    
    @JoinColumn(name = "gender", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Gender gender;
    
    @JoinColumn(name = "state_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private State stateId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "applicantId")
    private List<ApplicantField> applicantFieldList;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "applicantId")
    private List<ApplicantSkill> applicantSkillList;

    public Applicant() {
    }

    public Applicant(Integer id) {
        this.id = id;
    }

    public Applicant(Integer id, String emailId, String applicantNo, String firstName, String lastName, Date dob, Date registrationDate, String mobileNo, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.emailId = emailId;
        this.applicantNo = applicantNo;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.registrationDate = registrationDate;
        this.mobileNo = mobileNo;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getEmailId() {
    	return this.emailId;
    }
    
    public void setEmailId(String emailId) {
    	this.emailId = emailId;
    }

    public String getApplicantNo() {
        return applicantNo;
    }

    public void setApplicantNo(String applicantNo) {
        this.applicantNo = applicantNo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Country getCountryId() {
        return countryId;
    }

    public void setCountryId(Country countryId) {
        this.countryId = countryId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public State getStateId() {
        return stateId;
    }

    public void setStateId(State stateId) {
        this.stateId = stateId;
    }

    @XmlTransient
    public List<ApplicantField> getApplicantFieldList() {
        return applicantFieldList;
    }

    public void setApplicantFieldList(List<ApplicantField> applicantFieldList) {
        this.applicantFieldList = applicantFieldList;
    }

    @XmlTransient
    public List<ApplicantSkill> getApplicantSkillList() {
        return applicantSkillList;
    }

    public void setApplicantSkillList(List<ApplicantSkill> applicantSkillList) {
        this.applicantSkillList = applicantSkillList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Applicant)) {
            return false;
        }
        Applicant other = (Applicant) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.Applicant[ id=" + id + " ]";
    }
    
}
