/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_round")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Round.findAll", query = "SELECT r FROM Round r"),
    @NamedQuery(name = "Round.findById", query = "SELECT r FROM Round r WHERE r.id = :id"),
    @NamedQuery(name = "Round.findByName", query = "SELECT r FROM Round r WHERE r.name = :name"),
    @NamedQuery(name = "Round.findByDescription", query = "SELECT r FROM Round r WHERE r.description = :description"),
    @NamedQuery(name = "Round.findByDefaultDurationHours", query = "SELECT r FROM Round r WHERE r.defaultDurationHours = :defaultDurationHours"),
    @NamedQuery(name = "Round.findByDefaultDurationMinutes", query = "SELECT r FROM Round r WHERE r.defaultDurationMinutes = :defaultDurationMinutes"),
    @NamedQuery(name = "Round.findByCreatedAt", query = "SELECT r FROM Round r WHERE r.createdAt = :createdAt"),
    @NamedQuery(name = "Round.findByCreatedBy", query = "SELECT r FROM Round r WHERE r.createdBy = :createdBy"),
    @NamedQuery(name = "Round.findByModifiedAt", query = "SELECT r FROM Round r WHERE r.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "Round.findByModifiedBy", query = "SELECT r FROM Round r WHERE r.modifiedBy = :modifiedBy")})
public class Round implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "default_duration_hours")
    private int defaultDurationHours;
    @Basic(optional = false)
    @NotNull
    @Column(name = "default_duration_minutes")
    private int defaultDurationMinutes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    @Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;

    public Round() {
    }

    public Round(Integer id) {
        this.id = id;
    }

    public Round(Integer id, String name, int defaultDurationHours, int defaultDurationMinutes, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = id;
        this.name = name;
        this.defaultDurationHours = defaultDurationHours;
        this.defaultDurationMinutes = defaultDurationMinutes;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDefaultDurationHours() {
        return defaultDurationHours;
    }

    public void setDefaultDurationHours(int defaultDurationHours) {
        this.defaultDurationHours = defaultDurationHours;
    }

    public int getDefaultDurationMinutes() {
        return defaultDurationMinutes;
    }

    public void setDefaultDurationMinutes(int defaultDurationMinutes) {
        this.defaultDurationMinutes = defaultDurationMinutes;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Round)) {
            return false;
        }
        Round other = (Round) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.Round[ id=" + id + " ]";
    }
    
}
