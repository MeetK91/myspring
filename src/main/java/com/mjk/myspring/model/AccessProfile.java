/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
package com.mjk.myspring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Meet Kulkarni <kulkarni.meet@gmail.com>
 */
@Entity
@Table(name = "tbl_access_profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AccessProfile.findAll", query = "SELECT a FROM AccessProfile a"),
    @NamedQuery(name = "AccessProfile.findByUserProfileId", query = "SELECT a FROM AccessProfile a WHERE a.id = :id"),
    @NamedQuery(name = "AccessProfile.findByUserProfileType", query = "SELECT a FROM AccessProfile a WHERE a.type = :type"),
    @NamedQuery(name = "AccessProfile.findByCreatedAt", query = "SELECT a FROM AccessProfile a WHERE a.createdAt = :createdAt"),
    @NamedQuery(name = "AccessProfile.findByCreatedBy", query = "SELECT a FROM AccessProfile a WHERE a.createdBy = :createdBy"),
    @NamedQuery(name = "AccessProfile.findByModifiedAt", query = "SELECT a FROM AccessProfile a WHERE a.modifiedAt = :modifiedAt"),
    @NamedQuery(name = "AccessProfile.findByModifiedBy", query = "SELECT a FROM AccessProfile a WHERE a.modifiedBy = :modifiedBy")})
public class AccessProfile implements Serializable {
    
	private static final long serialVersionUID = 1L;
    
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "type")
    private String type;
    
	@Basic(optional = false)
    @NotNull
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;
    
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "created_by")
    private String createdBy;
    
	@Basic(optional = false)
    @NotNull
    @Column(name = "modified_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modifiedAt;
    
	@Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "modified_by")
    private String modifiedBy;
    
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "accessProfileId")
    private List<UserProfileMapping> userProfileMappingList;

    public AccessProfile() {
    }

    public AccessProfile(Integer id) {
        this.id = id;
    }

    public AccessProfile(Integer userProfileId, String userProfileType, Date createdAt, String createdBy, Date modifiedAt, String modifiedBy) {
        this.id = userProfileId;
        this.type = userProfileType;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.modifiedAt = modifiedAt;
        this.modifiedBy = modifiedBy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    @XmlTransient
    public List<UserProfileMapping> getUserProfileMappingList() {
        return userProfileMappingList;
    }

    public void setUserProfileMappingList(List<UserProfileMapping> userProfileMappingList) {
        this.userProfileMappingList = userProfileMappingList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AccessProfile)) {
            return false;
        }
        AccessProfile other = (AccessProfile) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mjk.myspring.model.AccessProfile[ userProfileId=" + id + " ]";
    }
    
}
