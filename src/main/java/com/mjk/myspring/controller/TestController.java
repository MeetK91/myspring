/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mjk.myspring.view.DataDisplayObject;
import com.mjk.myspring.view.component.DropdownOptionViewComponent;
import com.mjk.myspring.view.component.DropdownViewComponent;
import com.mjk.myspring.view.component.PlainTextViewComponent;
import com.mjk.myspring.view.component.TableCellViewComponent;
import com.mjk.myspring.view.component.TableRowViewComponent;
import com.mjk.myspring.view.component.TableViewComponent;

@Controller
public class TestController {

	@RequestMapping(path = "/test", method = RequestMethod.GET)
	public ModelAndView serveTest() {

		DataDisplayObject ddo = new DataDisplayObject();

		ddo.setDropdownMap(getDropdown());
		ddo.setTableMap(getTable());

		return new ModelAndView("test", "ddo", ddo);
	}

	private static Map<String, DropdownViewComponent> getDropdown() {
		Map<String, DropdownViewComponent> dropdownMap = new HashMap<String, DropdownViewComponent>();

		List<DropdownOptionViewComponent> countryOptions = new ArrayList<DropdownOptionViewComponent>();

		countryOptions.add(new DropdownOptionViewComponent("1", "India", false));
		countryOptions.add(new DropdownOptionViewComponent("2", "Pakistan", true));
		countryOptions.add(new DropdownOptionViewComponent("3", "China", false));
		countryOptions.add(new DropdownOptionViewComponent("4", "Japan", false));

		dropdownMap.put("countryDropdown", new DropdownViewComponent("country", false, countryOptions));

		List<DropdownOptionViewComponent> genderOptions = new ArrayList<DropdownOptionViewComponent>();

		genderOptions.add(new DropdownOptionViewComponent("1", "Male", false));
		genderOptions.add(new DropdownOptionViewComponent("2", "Female", true));
		genderOptions.add(new DropdownOptionViewComponent("3", "Transgender", false));

		dropdownMap.put("genderDropdown", new DropdownViewComponent("gender", false, genderOptions));

		return dropdownMap;
	}

	private static Map<String, TableViewComponent> getTable() {

		Map<String, TableViewComponent> tableMap = new HashMap<String, TableViewComponent>();

		List<TableRowViewComponent> rows = new ArrayList<TableRowViewComponent>();

		List<TableCellViewComponent> cells = new ArrayList<TableCellViewComponent>();

		
		for (int j = 0; j < 10; j++) {
			
			
			for (int i = 1; i <= 10; i++) {

				// id
				cells.add(new TableCellViewComponent(new PlainTextViewComponent(Integer.toString(i))));

				// name
				cells.add(new TableCellViewComponent(new PlainTextViewComponent(Integer.toString(i))));
			}
			
		}

		return tableMap;
	}
}