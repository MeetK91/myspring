/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mjk.myspring.model.Sidebar;
import com.mjk.myspring.service.ISidebarService;

@Controller
public class PageController {
	
	//TODO Not convinced with declaring sidebar here. There must be a better way to handle this (9e1f56b).
	@Autowired
	Sidebar sidebar;
	
	@Autowired
	ISidebarService sidebarService;
	
	@RequestMapping(value={"/sidebar"}, method=RequestMethod.GET)
	public ModelAndView getSidebar(HttpRequest request) {
		
		return new ModelAndView("sidebar","sidebar",getSidebarContent(true));
	}
	
	@RequestMapping(value={"/top"}, method=RequestMethod.GET)
	public String getTop() {
		return "top";
	}
	
	@RequestMapping(value={"/footer"}, method=RequestMethod.GET)
	public String getFooter() {
		return "footer";
	}
	
	@RequestMapping(value={"/header"}, method=RequestMethod.GET)
	public String getHeader() {
		return "header";
	}
	
	@RequestMapping(value={"/bottom"}, method=RequestMethod.GET)
	public String getBottom() {
		return "bottom";
	}
	
	@RequestMapping(value={"/license"}, method=RequestMethod.GET)
	public String getLicense() {
		return "license";
	}
	
	private Sidebar getSidebarContent(boolean isUserLoggedIn) {
		if(this.sidebar != null)
			return this.sidebar;
		else
			return sidebarService.getSidebar(isUserLoggedIn);
	}
	
}