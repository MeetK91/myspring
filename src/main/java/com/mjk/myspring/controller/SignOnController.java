/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.controller;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;

import com.mjk.myspring.dto.SignOnDto;
import com.mjk.myspring.model.User;
import com.mjk.myspring.security.TokenGenerator;
import com.mjk.myspring.service.IUserService;

@Controller
public class SignOnController {
	
	Logger logger = LoggerFactory.getLogger(SignOnController.class);
	
	@Autowired
	IUserService iUserService;
	
	@Autowired
	LocaleResolver localeResolver;
	
	@Autowired
	@Qualifier("signOnFormValidator")
	private Validator validator;
	
	@InitBinder
	private void initBinder(WebDataBinder binder){
		binder.setValidator(validator);
	}
	
	@RequestMapping(value = { "/signon" }, method = RequestMethod.GET)
	public String newUser(ModelMap model, HttpServletRequest request) {
		
		System.out.println("sysout Hash - " + TokenGenerator.INSTANCE.hashCode());
		System.out.println("sysout - " + TokenGenerator.INSTANCE.getNewStringToken());
		System.out.println("locale - " + localeResolver.resolveLocale(request));
		logger.debug(TokenGenerator.INSTANCE.getNewStringToken());
		logger.debug("Hash - " + TokenGenerator.INSTANCE.hashCode());
		
		return "signon";
	}
	
	@RequestMapping(value = { "/signon" }, method = RequestMethod.POST)
	public ModelAndView signUpUser(@ModelAttribute("signOnDto") @Validated SignOnDto signOnDto, BindingResult result, ModelMap model, HttpServletRequest request) {
		
		System.out.println("Signon attempt: "+signOnDto.getSignUpEmailId()+ " - " +signOnDto.getSignUpPassword()+ " - " +signOnDto.getSignUpConfirmPassword());
		
		logger.debug("Signon attempt: {} {} {}",signOnDto.getSignUpEmailId(),signOnDto.getSignUpPassword(),signOnDto.getSignUpConfirmPassword());
		
		if(result.hasErrors()){
			System.out.println("Result has errors!!");
			return new ModelAndView("signon","signOnDto",signOnDto);
		}
		
		User newUser;
		
		newUser = iUserService.registerNewUser(signOnDto); 
		
		if(newUser != null) {
			return new ModelAndView("redirect:/login?signon");
		}
		
		return new ModelAndView("signon");
	}
}