/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.mjk.myspring.model.User;
import com.mjk.myspring.service.IUserService;

@Controller
public class UserProfileController {
	
	@Autowired
	IUserService iUserService;

	Logger logger = LoggerFactory.getLogger(UserProfileController.class);
	
	@PreAuthorize("/profile")
	@RequestMapping(value={"/profile", "/index"}, method=RequestMethod.GET)
	public ModelAndView userPage(ModelMap model, Principal principal){
		
		if(principal != null) {
			logger.debug("Profile page requested - " + principal.getName());		
			User loggedInUser = iUserService.findByEmail(principal.getName());
			return new ModelAndView("applicant_profile", "applicant", loggedInUser);
		}
		else {
			return new ModelAndView("login");
		}
	}
}