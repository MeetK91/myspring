/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.mjk.myspring.model.AccessProfile;
//import com.mjk.myspring.model.User;
import com.mjk.myspring.service.IAccessProfileService;
import com.mjk.myspring.service.IUserService;

@Controller
@RequestMapping("/")
@SessionAttributes("roles")
public class LoginController {
	
	Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	IUserService iUserService;
	
	@Autowired
	IAccessProfileService iAccessProfileService;
	
	@Autowired
	MessageSource messageSource;

	@Autowired
	PersistentTokenBasedRememberMeServices persistentTokenBasedRememberMeServices;
	
	@Autowired
	AuthenticationTrustResolver authenticationTrustResolver;
	
	/**
	 * This method will provide UserProfile list to views
	 */
	@ModelAttribute("roles")
	public List<AccessProfile> initializeProfiles() {
		logger.trace("Entry/Exit: initializeProfiles()");
		return iAccessProfileService.findAll();
	}

	/**
	 * This method handles login GET requests.
	 * If users is already logged-in and tries to access login page again, will be redirected to list page.
	 */
	@RequestMapping(value = {"/","/login"}, method = RequestMethod.GET)
	public String loginPage() {
		logger.trace("Entry: /login.GET");
		if (isCurrentAuthenticationAnonymous()) {
			logger.trace("Exit: /login.GET");
			return "login";
	    } else {
	    	logger.trace("Exit: /login.GET");
	    	return "redirect:/profile";//TODO Try getting rid of redirect - check for alternatives.  
	    }
	}

	/**
	 * This method handles logout requests.
	 * Toggle the handlers if you are RememberMe functionality is useless in your app.
	 */
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logoutPage (HttpServletRequest request, HttpServletResponse response){
		logger.trace("Entry: /logout.GET");
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){
			//new SecurityContextLogoutHandler().logout(request, response, auth);
			persistentTokenBasedRememberMeServices.logout(request, response, auth);
			SecurityContextHolder.getContext().setAuthentication(null);
		}
		logger.trace("Exit : /logout.GET");
		return "redirect:/login?logout";
	}
	
	@RequestMapping(value="/forgot", method = RequestMethod.GET)
	public String forgotPassword(HttpServletRequest request, HttpServletResponse response){
		logger.trace("Entry/Exit: Controller forgotPassword()");
		return "redirect:/accessDenied";
	}
	
	@RequestMapping(value="/accessDenied", method = RequestMethod.GET)
	public String accessDenied(HttpServletRequest request, HttpServletResponse response){
		logger.trace("Entry/Exit: Controller accessDenied()");
		return "page403";
	}
	
	/**
	 * This method returns the principal[user-name] of logged-in user.
	 */
//	private String getPrincipal(){
//		String userName = null;
//		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//
//		if (principal instanceof UserDetails) {
//			userName = ((UserDetails)principal).getUsername();
//		} else {
//			userName = principal.toString();
//		}
//		
//		return userName;
//	}
	
	/**
	 * This method returns true if users is already authenticated [logged-in], else false.
	 */
	private boolean isCurrentAuthenticationAnonymous() {
		boolean ret;
		logger.trace("Entry: isCurrentAuthenticationAnonymous()");
	    final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
	    ret = authenticationTrustResolver.isAnonymous(authentication);
	    logger.trace("Exit: isCurrentAuthenticationAnonymous()");
	    return ret;
	}

}