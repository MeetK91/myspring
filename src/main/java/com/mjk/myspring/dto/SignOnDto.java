/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dto;

import java.util.regex.Pattern;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.mjk.myspring.validation.ValidationHelper;

public class SignOnDto {
	
	@NotNull
	@NotEmpty
	private String firstName;
	
	@NotNull
	@NotEmpty
	private String lastName;
	
	@NotNull
	@NotEmpty
	public String signUpEmailId;
	
	@NotNull
	@NotEmpty
	private String signUpPassword;
	
	@NotNull
	@NotEmpty
	private String signUpConfirmPassword;
	
	ValidationDto validationDto;
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName.trim();
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName.trim();
	}
	
	public String getSignUpEmailId() {
		return signUpEmailId;
	}
	
	public void setSignUpEmailId(String signUpEmailId) {
		this.signUpEmailId = signUpEmailId.trim();
	}
	
	public String getSignUpPassword() {
		return signUpPassword;
	}
	
	public void setSignUpPassword(String signUpPassword) {
		this.signUpPassword = signUpPassword;
	}
	
	public String getSignUpConfirmPassword() {
		return signUpConfirmPassword;
	}
	
	public void setSignUpConfirmPassword(String signUpConfirmPassword) {
		this.signUpConfirmPassword = signUpConfirmPassword;
	}
	
	private boolean validatePasswords() {
		
		if(ValidationHelper.isEmptyOrWhitespace(this.signUpPassword)) {
			validationDto.setError(true);
			validationDto.setField("password");
			validationDto.setFieldError("error.empty.password");
			
			return false;
		}
		
		if(ValidationHelper.isEmptyOrWhitespace(this.signUpConfirmPassword)) {
			validationDto.setError(true);
			validationDto.setField("confirmPassword");
			validationDto.setFieldError("error.empty.password");
			
			return false;
		}
		
		if(!this.signUpPassword.equals(this.signUpConfirmPassword)) {
			validationDto.setError(true);
			validationDto.setField("confirmPassword");
			validationDto.setFieldError("error.mismatch.password");
			
			return false;
		}
		
		return true;
		
	}
	
	private String getPasswordStrength() {
		return "STRONG";
	}
	
	private void validatePasswordStrength(String password){
		
		if(!this.validatePasswords()) {
			return;
		}
		
		final int MIN_PASSWORD_LENGTH = 8;
		final int MAX_PASSWORD_LENGTH = 20;
		
		Pattern specialCharPattern = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
	    Pattern upperCasePattern = Pattern.compile("[A-Z ]");
	    Pattern lowerCasePattern = Pattern.compile("[a-z ]");
	    Pattern numberPattern = Pattern.compile("[0-9 ]");
		
		switch(this.getPasswordStrength()){

    	default:
    
    	case "STRONG":
    		if(!specialCharPattern.matcher(password).find()){
    			validationDto.setError(true);
    			validationDto.setField("password");
    			validationDto.setFieldError("error.strength.password");
    			
    			return;
    		}

    	case "MODERATE":
    		if(!numberPattern.matcher(password).find()){
    			validationDto.setError(true);
    			validationDto.setField("password");
    			validationDto.setFieldError("error.strength.password");
    			
    			return;
    		}
    		
    	case "POOR":
    		if(!upperCasePattern.matcher(password).find()){
    			validationDto.setError(true);
    			validationDto.setField("password");
    			validationDto.setFieldError("error.strength.password");
    			
    			return;
    		}
    		
    		if(!lowerCasePattern.matcher(password).find()){
    			validationDto.setError(true);
    			validationDto.setField("password");
    			validationDto.setFieldError("error.strength.password");
    			
    			return;
    		}
    		
    		if(!ValidationHelper.isNumberWithinRange(password.length(), MIN_PASSWORD_LENGTH, MAX_PASSWORD_LENGTH)){
    			validationDto.setError(true);
    			validationDto.setField("password");
    			validationDto.setFieldError("error.strength.password");
    			
    			return;
    		}
		}
		
		return;
	}
	
	public ValidationDto validate() {
		
		validationDto = new ValidationDto();
		
		if(ValidationHelper.isEmptyOrWhitespace(this.firstName)) {
			validationDto.setError(true);
			validationDto.setField("firstName");
			validationDto.setFieldError("error.empty.firstname");
		}
		
		if(ValidationHelper.isEmptyOrWhitespace(this.lastName)) {
			validationDto.setError(true);
			validationDto.setField("lastName");
			validationDto.setFieldError("error.empty.lastname");
		}
		
		if(ValidationHelper.isEmptyOrWhitespace(this.signUpEmailId)) {
			validationDto.setError(true);
			validationDto.setField("signUpEmailId");
			validationDto.setFieldError("error.empty.email");
		}
		
		if(ValidationHelper.isEmptyOrWhitespace(this.signUpPassword)) {
			validationDto.setError(true);
			validationDto.setField("signUpPassword");
			validationDto.setFieldError("error.empty.password");
		}
		
		if(ValidationHelper.isEmptyOrWhitespace(this.signUpConfirmPassword)) {
			validationDto.setError(true);
			validationDto.setField("signUpConfirmPassword");
			validationDto.setFieldError("error.empty.password");
		}
		
		if(!ValidationHelper.isEmailValid(signUpEmailId)) {
			validationDto.setError(true);
			validationDto.setField("signUpEmailId");
			validationDto.setFieldError("error.invalid.email");
		}
		
		this.validatePasswordStrength(this.signUpPassword);
		
		validationDto.setError(false);
		return validationDto;
	}
}