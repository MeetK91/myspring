/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.mjk.myspring.model.Skill;

public class UserProfileDto {
	
	@NotNull
	@NotEmpty
	String firstName;
	
	@NotNull
	@NotEmpty
	String lastName;
	
	@NotNull
	@NotEmpty
	String primaryEmail;
	
	@NotNull
	List<Skill> skillList;

}