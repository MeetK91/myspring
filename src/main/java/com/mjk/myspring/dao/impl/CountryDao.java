/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.ICountryDao;
import com.mjk.myspring.model.Country;

@Repository("countryDao")
public class CountryDao extends AbstractDao<Integer, Country> implements ICountryDao {

	@Override
	public Country find(String shortCode) {
		
		return (Country) entityManager.createQuery("SELECT c FROM Country c WHERE c.shortCode LIKE :shortCode")
				.setParameter("shortCode", shortCode)
				.getSingleResult();
	}

	@Override
	public void save(Country country) {
		entityManager.persist(country);
	}

}
