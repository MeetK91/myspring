/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IPrivilegeDao;
import com.mjk.myspring.model.Privilege;

@Repository("privilegeDao")
public class PrivilegeDao extends AbstractDao<Integer, Privilege> implements IPrivilegeDao {
	
	Logger logger = LoggerFactory.getLogger(PrivilegeDao.class);

	@Override
	public Privilege findByPath(String path) {
		logger.trace("Entry: findByPath()");
		return entityManager
				.createQuery("SELECT p FROM Privilege p WHERE p.path = :path", Privilege.class)
				.setParameter("path", path)
				.getSingleResult();
	}

	@Override
	public void save(Privilege privilege) {
		this.persist(privilege);
	}

}