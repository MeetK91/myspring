/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IRoleDao;
import com.mjk.myspring.model.Role;
import com.mjk.myspring.model.RoleType;

@Repository("roleDao")
public class RoleDao extends AbstractDao<Integer, Role> implements IRoleDao {

	@Override
	public Role findByName(String name) {
		return entityManager
				.createQuery("SELECT r FROM Role r WHERE r.name = :name", Role.class)
				.setParameter("name", name)
				.getSingleResult();
	}

	@Override
	public List<Role> findByType(RoleType roleType) {
		return entityManager.createNamedQuery("Role.findByRoleType", Role.class)
				.setParameter("roleType", roleType)
				.getResultList();
	}

}