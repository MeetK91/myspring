/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IStateDao;
import com.mjk.myspring.model.State;

@Repository("stateDao")
public class StateDao extends AbstractDao<Integer, State> implements IStateDao {

	@Override
	public State find(String shortCode) {
		return (State) entityManager.createQuery("SELECT s FROM State s WHERE s.shortCode = :shortCode")
				.setParameter("shortCode", shortCode)
				.getSingleResult();
	}

}