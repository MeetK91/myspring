/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IApplicantDao;
import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;

@Repository("applicantDao")
public class ApplicantDao extends AbstractDao<Integer, Applicant> implements IApplicantDao {

	@Override
	public Applicant find(int id) {
		Applicant applicant = getByKey(id);
		return ((applicant == null) ? null : applicant);
	}
	
	@Override
	public List<Applicant> find() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Applicant find(User user) {
		return find(user.getPrimaryEmail());
	}
	
	@Override
	public Applicant find(Applicant applicant) {
		return find(applicant.getEmailId());
	}
	
	@Override
	public Applicant find(String emailId) {
		return entityManager.createQuery("SELECT a FROM Applicant a WHERE a.emailId = :emailId", Applicant.class)
				.setParameter("emailId", emailId)
				.getSingleResult();
	}

	@Override
	public void save(Applicant applicant) {
		this.persist(applicant);
	}

	@Override
	public void modify(Applicant applicant) {
		// TODO Auto-generated method stub

	}
	
	@Override
	public void remove(Applicant applicant){
		this.delete(applicant);
	}

}
