/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IAccessProfileDao;
import com.mjk.myspring.model.AccessProfile;

@Repository("accessProfileDao")
public class AccessProfileDao extends AbstractDao<Integer, AccessProfile> implements IAccessProfileDao {

	Logger logger = LoggerFactory.getLogger(AccessProfileDao.class);
	
	@Override
	public List<AccessProfile> findAll() {
		try {
			logger.trace("Entry: findAll()");
			return entityManager.createQuery("SELECT a FROM AccessProfile a", AccessProfile.class).getResultList();
		}
		catch(IllegalArgumentException exc){
			logger.error("Unable to fetch AccessProfile list.");
			logger.debug(exc.toString());
			return null;
		}
		finally {
			logger.trace("Exit: findAll()");
		}
	}

	@Override
	public AccessProfile findByType(String type) {
		try {
			logger.info("Entry: findByType()");
			return entityManager
					.createQuery("SELECT a FROM AccessProfile a WHERE a.type = :type", AccessProfile.class)
					.setParameter("type", type)
					.getSingleResult();
		}
		catch(IllegalArgumentException exc) {
			logger.info("Unable to retrive AccessProfile.");
			logger.debug(exc.toString());
			return null;
		}
		finally {
			logger.trace("Exit: findByType()");
		}
	}

	@Override
	public AccessProfile findById(int id) {
		logger.info("Entry/Exit: findById()");
		return getByKey(id);
	}

	@Override
	public void save(AccessProfile profile) {
		logger.info("Entry/Exit: save()");
		persist(profile);
	}

}