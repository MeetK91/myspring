/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

/**
 * 
 */
package com.mjk.myspring.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IMenuDao;
import com.mjk.myspring.model.Menu;

/**
 * Class implementing the IMenuDao interface.
 * 
 * @author Meet Kulkarni
 *
 */

@Repository("menuDao")
public class MenuDao implements IMenuDao {

	/**
	 * @see com.mjk.myspring.dao.IMenuDao#getMenus(boolean)
	 */
	@Override
	public List<Menu> getMenus(boolean isUserLoggedIn) {
		// TODO Auto-generated method stub
		return null;
	}

}
