/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IRoleTypeDao;
import com.mjk.myspring.model.RoleType;

@Repository("roleTypeDao")
public class RoleTypeDao extends AbstractDao<Integer, RoleType> implements IRoleTypeDao {

	@Override
	public List<RoleType> getAllRoleTypes() {
		return entityManager.createNamedQuery("RoleType.findAll", RoleType.class).getResultList();
	}

}