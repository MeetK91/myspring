/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mjk.myspring.dao.IUserDao;
import com.mjk.myspring.model.User;

@Repository("userDao")
public class UserDao extends AbstractDao<Integer, User> implements IUserDao {
	
	static final Logger logger = LoggerFactory.getLogger(UserDao.class);

	@Override
	public User findById(int id) {
		User user = getByKey(id);
		if(user!=null){
			Hibernate.initialize(user.getUserProfileMappingList());
		}
		return user;
	}

	@Override
	public User findByEmail(String email) {
		try {
			User user;
			List<User> userList = entityManager
					.createQuery("SELECT u FROM User u WHERE u.primaryEmail = :email", User.class)
					.setParameter("email", email)
					.getResultList();
		
			if(userList.size() > 0){
				user = userList.get(0);
				Hibernate.initialize(user.getUserProfileMappingList());
				return user;
			}
			
			return null;
		}
		catch(HibernateException exc) {
			logger.error("User not found.");
			logger.debug(exc.toString());
			return null;
		}
		finally {
			logger.trace("Exit: findByEmail()");
		}
	}

	@Override
	public void save(User user) {
		this.persist(user);
	}

	@Override
	public void deleteByEmail(String email) {
		this.delete(this.findByEmail(email));
	}

	@Override
	public List<User> findAllUsers() {
		
		try {
			logger.trace("Entry: findAllUsers()");
			return entityManager
					.createQuery("SELECT u FROM User u", User.class)
					.getResultList();
		}
		catch(IllegalArgumentException exc){
			logger.error("Users not found.");
			logger.debug(exc.toString());
			return null;
		}
		finally{
			logger.trace("Exit: findAllUsers()");
		}
	}
	
	@Override
	public int newId() {
		return entityManager.createNamedQuery("User.lastId", User.class).getSingleResult().getId();
	}

}