/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.dao;

import java.util.List;

import com.mjk.myspring.model.Applicant;
import com.mjk.myspring.model.User;

public interface IApplicantDao {
	
	Applicant find(int id);
	
	List<Applicant> find();
	
	Applicant find(User user);
	
	Applicant find(String emailId);
	
	Applicant find(Applicant applicant);
	
	void save(Applicant applicant);
	
	void modify(Applicant applicant);
	
	void remove(Applicant applicant);

}
