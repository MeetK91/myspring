/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.util;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.mjk.myspring.dao.impl.PrivilegeDao;
import com.mjk.myspring.dao.impl.RoleDao;
import com.mjk.myspring.dao.impl.UserDao;
import com.mjk.myspring.model.Privilege;
import com.mjk.myspring.model.Role;
import com.mjk.myspring.model.User;

@Component
public class InitialSecurityDataLoader implements ApplicationListener<ContextRefreshedEvent> {
 
    boolean alreadySetup = true;
 
    @Autowired
    private UserDao userDao;
  
    @Autowired
    private RoleDao roleDao;
  
    @Autowired
    private PrivilegeDao privilegeDao;
  
    @Autowired
    private PasswordEncoder passwordEncoder;
  
    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {
  
        if (alreadySetup)
            return;
        Privilege readPrivilege
          = createPrivilegeIfNotFound("READ_PRIVILEGE");
        Privilege writePrivilege
          = createPrivilegeIfNotFound("WRITE_PRIVILEGE");
  
        List<Privilege> adminPrivileges = Arrays.asList(
          readPrivilege, writePrivilege);        
        createRoleIfNotFound("ROLE_ADMIN", adminPrivileges);
        createRoleIfNotFound("ROLE_USER", Arrays.asList(readPrivilege));
 
        Role adminRole = roleDao.findByName("ROLE_ADMIN");
        User user = new User();
        user.setPassword(passwordEncoder.encode("test"));
        user.setPrimaryEmail("test@test.com");
        user.setRoles(Arrays.asList(adminRole));
        user.setIsActivated(true);
        userDao.save(user);
 
        alreadySetup = true;
    }
 
    @Transactional
    private Privilege createPrivilegeIfNotFound(String path) {
  
        Privilege privilege = privilegeDao.findByPath(path);
        if (privilege == null) {
            privilege = new Privilege(path);
            privilegeDao.save(privilege);
        }
        return privilege;
    }
 
    @Transactional
    private Role createRoleIfNotFound(
      String name, List<Privilege> privileges) {
  
        Role role = roleDao.findByName(name);
        if (role == null) {
            role = new Role(name);
            role.setPrivileges(privileges);
            roleDao.persist(role);
        }
        return role;
    }
}