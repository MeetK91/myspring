/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view;

import java.util.Map;

import com.mjk.myspring.view.component.CheckboxViewComponent;
import com.mjk.myspring.view.component.DropdownViewComponent;
import com.mjk.myspring.view.component.RadioViewComponent;
import com.mjk.myspring.view.component.TableViewComponent;
import com.mjk.myspring.view.component.TextboxViewComponent;

public class DataDisplayObject {

	Map<String, DropdownViewComponent> dropdownMap;
	Map<String, TextboxViewComponent> textboxMap;
	Map<String, CheckboxViewComponent> checkboxMap;
	Map<String, RadioViewComponent> radioMap;
	Map<String, TableViewComponent> tableMap;

	public Map<String, DropdownViewComponent> getDropdownMap() {
		return dropdownMap;
	}

	public void setDropdownMap(Map<String, DropdownViewComponent> dropdownMap) {
		this.dropdownMap = dropdownMap;
	}

	public Map<String, TextboxViewComponent> getTextboxMap() {
		return textboxMap;
	}

	public void setTextboxMap(Map<String, TextboxViewComponent> textboxMap) {
		this.textboxMap = textboxMap;
	}

	public Map<String, CheckboxViewComponent> getCheckboxMap() {
		return checkboxMap;
	}

	public void setCheckboxMap(Map<String, CheckboxViewComponent> checkboxMap) {
		this.checkboxMap = checkboxMap;
	}

	public Map<String, RadioViewComponent> getRadioMap() {
		return radioMap;
	}

	public void setRadioMap(Map<String, RadioViewComponent> radioMap) {
		this.radioMap = radioMap;
	}

	public Map<String, TableViewComponent> getTableMap() {
		return tableMap;
	}

	public void setTableMap(Map<String, TableViewComponent> tableMap) {
		this.tableMap = tableMap;
	}

}