/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

import java.util.ArrayList;
import java.util.List;

public class RadioViewComponent extends MyViewComponent {

	List<RadioOptionViewComponent> options;

	/**
	 * Initializes the options array as an empty ArrayList.
	 */
	public RadioViewComponent() {
		super();
		this.options = new ArrayList<RadioOptionViewComponent>();
	}

	/**
	 * 
	 * @param List<RadioOptionComponent>
	 */
	public RadioViewComponent(List<RadioOptionViewComponent> options) {
		super();
		this.options = options;
	}

	/**
	 * 
	 * @return List<RadioOptionComponent>
	 */
	public List<RadioOptionViewComponent> getOptions() {
		return options;
	}

	/**
	 * 
	 * @param options
	 */
	public void setOptions(List<RadioOptionViewComponent> options) {
		this.options = options;
	}
	
}