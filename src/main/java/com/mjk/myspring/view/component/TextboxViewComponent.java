/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

public class TextboxViewComponent extends MyViewComponent {
	
	String text;
	boolean readOnly;
	String type;
	
	public TextboxViewComponent() {
		super();
		this.text = "";
		this.type = "input";
	}

	/**
	 * Initializes the textbox 
	 * @param String id
	 * @param String text
	 */
	public TextboxViewComponent(String id, String text) {
		super(id);
		this.text = text;
		this.type = "text";
	}
	
	public TextboxViewComponent(String id, String type, String text) {
		super(id);
		this.type = type;
		this.text = text;
	}
	
	public TextboxViewComponent(String id, boolean disabled, String text) {
		super(id, disabled);
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}