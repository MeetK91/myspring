/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

import java.util.ArrayList;
import java.util.List;

public class CheckboxViewComponent extends MyViewComponent {
	
	List<CheckboxOptionViewComponent> options;
	
	/**
	 * Initializes the options array as an empty ArrayList.
	 */
	public CheckboxViewComponent() {
		super();
		this.options = new ArrayList<CheckboxOptionViewComponent>();
	}

	public CheckboxViewComponent(List<CheckboxOptionViewComponent> options) {
		super();
		this.options = options;
	}

	public List<CheckboxOptionViewComponent> getOptions() {
		return options;
	}

	public void setOptions(List<CheckboxOptionViewComponent> options) {
		this.options = options;
	}

}