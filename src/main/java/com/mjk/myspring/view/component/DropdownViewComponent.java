/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

import java.util.List;

public class DropdownViewComponent extends MyViewComponent {
	
	List<DropdownOptionViewComponent> options;
	
	public DropdownViewComponent(List<DropdownOptionViewComponent> options) {
		this.options = options;
	}
	
	public DropdownViewComponent(String id, boolean disabled, List<DropdownOptionViewComponent> options) {
		super(id, disabled);
		this.options = options;
	}
	
	public String getId() {
		return this.id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public boolean getDisabled() {
		return this.disabled;
	}
	
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public List<DropdownOptionViewComponent> getOptions() {
		return options;
	}

	public void setOptions(List<DropdownOptionViewComponent> options) {
		this.options = options;
	}

}