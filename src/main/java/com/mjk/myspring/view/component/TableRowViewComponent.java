/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

import java.util.List;

public class TableRowViewComponent extends MyViewComponent {
	
	List<MyViewComponent> row;

	public List<MyViewComponent> getRow() {
		return row;
	}

	public void setRow(List<MyViewComponent> row) {
		this.row = row;
	}

}