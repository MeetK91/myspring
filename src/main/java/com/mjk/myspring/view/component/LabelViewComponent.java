/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

public class LabelViewComponent extends MyViewComponent {
	
	String text;
	String messageType;
	
	public LabelViewComponent(String text) {
		super();
		this.text = text;
		this.messageType = "";
	}

	public LabelViewComponent(String text, String messageType) {
		super();
		this.text = text;
		this.messageType = messageType;
	}

	public String getText() {
		return text;
	}
	
	public void setText(String text) {
		this.text = text;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
}