/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

import java.util.List;

public class TableViewComponent extends MyViewComponent {
	
	List<TableRowViewComponent> rows;

	public TableViewComponent(List<TableRowViewComponent> rows) {
		super();
		this.rows = rows;
	}
	
	public TableViewComponent(String id, List<TableRowViewComponent> rows) {
		super(id);
		this.rows = rows;
	}

	public List<TableRowViewComponent> getRows() {
		return rows;
	}

	public void setRows(List<TableRowViewComponent> rows) {
		this.rows = rows;
	}

}