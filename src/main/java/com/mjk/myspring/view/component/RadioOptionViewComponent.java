/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

public class RadioOptionViewComponent extends MyViewComponent {

	String name;
	String value;
	
	/**
	 * 
	 * @param String name
	 * @param String value
	 */
	public RadioOptionViewComponent(String name, String value) {
		super();
		this.name = name;
		this.value = value;
	}
	
	/**
	 * 
	 * @param String id
	 * @param String name
	 * @param String value
	 */
	public RadioOptionViewComponent(String id, String name, String value) {
		super(id);
		this.name = name;
		this.value = value;
	}

	/**
	 * 
	 * @return String name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param String name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return String value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * 
	 * @param String value
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
}