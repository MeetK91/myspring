/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/ or
	* send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.view.component;

public abstract class MyViewComponent {

	String id;
	String uriPath;
	boolean disabled;
	int tabOrder;
	String cssClass;
	
	/**
	 * Creates a ViewComponent with its disabled property set to false.
	 */
	public MyViewComponent() {
		this.disabled = false;
	}
	
	/**
	 * Creates a ViewComponent with the given String as its ID and sets its disabled property to false.
	 * @param String id
	 * 
	 */
	public MyViewComponent(String id) {
		this.id = id;
		this.disabled = false;
	}
	
	/**
	 * Creates a ViewComponent with its disabled property set as per the value provided in the parameter.
	 * @param boolean disabled
	 */
	public MyViewComponent(boolean disabled) {
		this.disabled = disabled;
	}
	
	/**
	 * Creates a ViewComponent with the given String as its ID and sets its disabled property as per the value provided in the parameter.
	 * @param String id
	 * @param boolean disabled
	 */
	public MyViewComponent(String id, boolean disabled) {
		this.id = id;
		this.disabled = disabled;
	}
	
	/**
	 * 
	 * @param disabled
	 * @param cssClass
	 */
	public MyViewComponent(boolean disabled, String cssClass) {
		this.disabled = disabled;
		this.cssClass = cssClass;
	}
	
	/**
	 * 
	 * @param id
	 * @param disabled
	 * @param cssClass
	 */
	public MyViewComponent(String id, boolean disabled, String cssClass) {
		this.id = id;
		this.disabled = disabled;
		this.cssClass = cssClass;
	}
	
	/**
	 * 
	 * @param id
	 * @param disabled
	 * @param tabOrder
	 * @param cssClass
	 */
	public MyViewComponent(String id, boolean disabled, int tabOrder, String cssClass) {
		this.id = id;
		this.disabled = disabled;
		this.tabOrder = tabOrder;
		this.cssClass = cssClass;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public int getTabOrder() {
		return tabOrder;
	}

	public void setTabOrder(int tabOrder) {
		this.tabOrder = tabOrder;
	}

	public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	
}