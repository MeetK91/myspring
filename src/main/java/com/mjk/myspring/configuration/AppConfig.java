/**
	* Copyright 2017 Meet Kulkarni <kulkarni.meet@gmail.com>.
	*
	* This work is licensed under the Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License.
	* To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/
	* or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

package com.mjk.myspring.configuration;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.format.FormatterRegistry;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import com.mjk.myspring.util.RoleToUserProfileConverter;

/**
 * 
 * @author mjkulkarni
 * 
 */
@Configuration
@Import(HibernateConfiguration.class)
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages = "com.mjk.myspring")
public class AppConfig extends WebMvcConfigurerAdapter {

	Logger logger = LoggerFactory.getLogger(AppConfig.class);
	
	@Autowired
	RoleToUserProfileConverter roleToUserProfileConverter;

	/**
	 * Configure ViewResolvers to deliver preferred views.
	 * 
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#configureViewResolvers(org.springframework.web.servlet.config.annotation.ViewResolverRegistry)
	 * @param ViewResolverRegistry
	 * @return void
	 */
	@Override
	public void configureViewResolvers(ViewResolverRegistry registry) {

		logger.info("Entry: configureViewResolvers()");
		
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setViewClass(JstlView.class);
		viewResolver.setPrefix("/WEB-INF/views/");
		viewResolver.setSuffix(".jsp");
		registry.viewResolver(viewResolver);
		
//		InternalResourceViewResolver viewComponentResolver = new InternalResourceViewResolver();
//		viewComponentResolver.setViewClass(JstlView.class);
//		viewComponentResolver.setPrefix("/WEB-INF/views/components/");
//		viewComponentResolver.setSuffix(".jsp");
//		registry.viewResolver(viewComponentResolver);
		
		logger.info("Exit: configureViewResolvers()");
	}

	/**
	 * Configure MessageSource to lookup any validation/error message in
	 * internationalized property files. Ensure the correct path (including filename but not file-extension) is
	 * properly specified. Spring will automatically append the locale identifier (en, fr, etc.) and
	 * ".properties" extenstion to the provided filename.
	 * 
	 * @param None
	 * @return messageSource with baseName property set.
	 */
	@Bean
	public MessageSource messageSource() {
		
		logger.info("Entry: messageSource()");
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("/static/messages");
		messageSource.setDefaultEncoding("UTF-8");
		logger.info("Exit: messageSource()");
		return messageSource;
	}
	
	/**
	 * Adds a locale resolver to handle different locales. We are using SessionLocaleResolver since we will be 
	 * handling user sessions. If sessions are not being used, we can also use CookieLocaleResolver; in this case 
	 * additional properties need to be specified. Google locale resolver for more information.
	 * 
	 * @param None
	 * @return LocaleResolver
	 */	
	@Bean(name="localeResolver")
	public LocaleResolver localeResolver(){
		
		logger.info("Entry: localeResolver()");
		SessionLocaleResolver resolver = new SessionLocaleResolver ();
		resolver.setDefaultLocale(new Locale("en"));
		logger.info("Exit: localeResolver()");
		return resolver;
	}
	
	/**
	 * Configure ResourceHandlers to serve static resources like CSS/Javascript
	 * etc.
	 * 
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addResourceHandlers(org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry)
	 * @param ResourceHandlerRegistry
	 * @return void
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		logger.info("Entry: addResourceHandlers()");
		registry.addResourceHandler("/static/**").addResourceLocations("/static/");
		logger.info("Exit: addResourceHandlers()");
	}
	
	/**
	 * 
	 * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter#addInterceptors(org.springframework.web.servlet.config.annotation.InterceptorRegistry)
	 * 
	 * Adding this Interceptor allows us to handle locale change.
	 * TODO Check if this works mid-session.
	 * 
	 * @param InterceptorRegistry
	 * @return void
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		logger.info("Entry: addInterceptors()");
	    LocaleChangeInterceptor interceptor = new LocaleChangeInterceptor();
	    interceptor.setParamName("locale");
	    registry.addInterceptor(interceptor);
	    logger.info("Exit: addInterceptors()");
	}

	/**
	 * Configure Converter to be used. In our example, we need a converter to
	 * convert string values[Roles] to UserProfiles in newUser.jsp
	 */
	@Override
	public void addFormatters(FormatterRegistry registry) {
		logger.info("Entry: addFormatters()");
		registry.addConverter(roleToUserProfileConverter);
		logger.info("Exit: addFormatters()");
	}
}