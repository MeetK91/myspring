<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<sec:authorize access="hasAuthority('/top/options')">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="glyphicon glyphicon-user"aria-hidden="true">
						</span>
					</a>
					<ul class="dropdown-menu">
						<sec:authorize access="hasAuthority('/top/options/profile')">
						<li>
							<a href="javascript:;">
								<i class="fa fa-user pull-right"></i> <strong> Profile</strong>
							</a>
						</li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('/top/options/settings')">
						<li>
							<a href="javascript:;">
								<i class="fa fa-cog pull-right"></i> <strong> Settings</strong>
							</a>
						</li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('/top/options/help')">
						<li>
							<a href="javascript:;">
								<i class="fa fa-question pull-right"></i> <strong> Help</strong>
							</a>
						</li>
						</sec:authorize>
						<sec:authorize access="hasAuthority('/top/options/logout')">
						<li>
							<a href="logout">
								<i class="fa fa-sign-out pull-right"></i> <strong> <spring:message code="label.logout" /></strong>
							</a>
						</li>
						</sec:authorize>
					</ul>
				</li>
				</sec:authorize>
				<sec:authorize access="hasAuthority('/top/alerts')">
				<li role="presentation" class="dropdown">
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false"> 
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-green">6</span>
					</a>
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
						<li>
							<a>
								<span class="image">
									<img src="images/img.jpg" alt="Profile Image" />
								</span>
								<span>
									<span>John Smith</span>
									<span class="time">3 mins ago</span>
								</span>
								<span class="message">
									Film festivals used to be do-or-die moments for movie makers. They were where...
								</span>
							</a>
						</li>
						<li><a> <span class="image"><img
									src="images/img.jpg" alt="Profile Image" /></span> <span> <span>John
										Smith</span> <span class="time">3 mins ago</span>
							</span> <span class="message"> Film festivals used to be
									do-or-die moments for movie makers. They were where... </span>
						</a></li>
						<li><a> <span class="image"><img
									src="images/img.jpg" alt="Profile Image" /></span> <span> <span>John
										Smith</span> <span class="time">3 mins ago</span>
							</span> <span class="message"> Film festivals used to be
									do-or-die moments for movie makers. They were where... </span>
						</a></li>
						<li><a> <span class="image"><img
									src="images/img.jpg" alt="Profile Image" /></span> <span> <span>John
										Smith</span> <span class="time">3 mins ago</span>
							</span> <span class="message"> Film festivals used to be
									do-or-die moments for movie makers. They were where... </span>
						</a></li>
						<li>
							<div class="text-center">
								<a> <strong>See All Alerts</strong> <i
									class="fa fa-angle-right"></i>
								</a>
							</div>
						</li>
					</ul></li>
					</sec:authorize>
			</ul>
		</nav>
	</div>
</div>