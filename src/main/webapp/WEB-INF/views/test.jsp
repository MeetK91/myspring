<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Hell yeah!</title>
	</head>
	<c:set var="dropdownMap" value="${ddo.dropdownMap}" scope="request"/>
	<c:set var="tableMap" value="${ddo.tableMap}" scope="request"/>
	<body>
		<jsp:include page="components/dropdownViewComponent.jsp">
			 <jsp:param name="dropdown" value="countryDropdown"/>
		</jsp:include>
		<jsp:include page="components/dropdownViewComponent.jsp">
			 <jsp:param name="dropdown" value="genderDropdown"/>
		</jsp:include>
		<jsp:include page="components/tableViewComponent.jsp">
			<jsp:param name="table" value="memberTable"/>
		</jsp:include>
	</body>
</html>