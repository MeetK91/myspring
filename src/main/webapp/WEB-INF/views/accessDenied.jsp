<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Access Denied!</title>

<!-- i18n -->
<spring:message code="label.access.denied" var="i18nLabelAccessDenied"/>
<spring:message code="error.denied.access" var="i18nMessageAccessDenied"/>
<spring:message code="label.logout" var="i18nLogout"/>

<!-- Controller URIs -->
<c:url value="/static/images" var="images"/>

<!-- Style URLs -->
<c:url value="/static/vendors/font-awesome/css/font-awesome.min.css" var="fontAwesomeCss" />
<c:url value="/static/vendors/bootstrap/dist/css/bootstrap.min.css" var="bootstrapCss" />
<c:url value="/static/vendors/animate.css/animate.min.css" var="animateCss" />
<c:url value="/static/vendors/nprogress/nprogress.css" var="nProgressCss" />
<c:url value="/static/build/css/custom.min.css" var="customThemeCss" />

<!-- Javascripts -->
<c:url value="/static/vendors/jquery/dist/jquery.min.js" var="jQueryMinJS"/>
<c:url value="/static/vendors/nprogress/nprogress.js" var="nProgressJS"/>
<c:url value="/static/vendors/bootstrap/dist/js/bootstrap.min.js" var="bootstrapMinJS"/>
<c:url value="/static/vendors/fastclick/lib/fastclick.js" var="fastClickJS"/>
<c:url value="/static/build/js/custom.min.js" var="customThemeJS"/>

<!-- Bootstrap -->
<link href="${bootstrapCss}" rel="stylesheet">

<!-- Font Awesome -->
<link href="${fontAwesomeCss}" rel="stylesheet">

<!-- NProgress -->
<link href="${nProgressCss}" rel="stylesheet">

<!-- Animate.css -->
<link href="${animateCss}" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="${customThemeCss}" rel="stylesheet">

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <!-- page content -->
        <div class="col-md-12">
          <div class="col-middle">
            <div class="text-center text-center">
              <h1 class="alert alert-danger">${i18nLabelAccessDenied} MESSAGE</h1>
              <h2 class="alert alert-danger">${i18nMessageAccessDenied}</h2>
            </div>
          </div>
          <%@include file="license.jsp"%>
        </div>
        <!-- /page content -->
      </div>
    </div>
    
	<script src="${jQueryMinJS}"></script>
	<script src="${nProgressJS}"></script>
	<script src="${bootstrapMinJS}"></script>
	<script src="${fastClickJS}"></script>
	<script src="${customThemeJS}"></script>

  </body>
</html>