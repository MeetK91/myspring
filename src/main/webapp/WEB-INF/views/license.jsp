<a rel="license"
	href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img
	alt="Creative Commons License" style="border-width: 0"
	src="${images}/license-s.png" /></a>

<i class="fa fa-copyright"></i>
<spring:message code="message.copyright" />
		 
