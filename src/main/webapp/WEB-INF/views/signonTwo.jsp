<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Sign On</title>

<!--  Style URLs -->
<c:url value="/static/vendors/font-awesome/css/font-awesome.min.css" var="fontAwesomeCss" />
<c:url value="/static/vendors/bootstrap/dist/css/bootstrap.min.css" var="bootstrapCss" />
<c:url value="/static/vendors/animate.css/animate.min.css" var="animateCss" />
<c:url value="/static/vendors/nprogress/nprogress.css" var="nProgressCss" />
<c:url value="/static/build/css/custom.min.css" var="customThemeCss" />

<!-- Controller URIs -->
<c:url value="/login" var="loginUrl" />
<c:url value="/signon" var="signOnUrl" />

<!-- Bootstrap -->
<link href="${bootstrapCss}" rel="stylesheet">

<!-- Font Awesome -->
<link href="${fontAwesomeCss}" rel="stylesheet">

<!-- NProgress -->
<link href="${nProgressCss}" rel="stylesheet">

<!-- Animate.css -->
<link href="${animateCss}" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="${customThemeCss}" rel="stylesheet">

</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
				<form:form class="login-form" method="post" action="${signOnUrl}">
					<h1>Create Account</h1>
					<div>
						<input type="text" name="userName" value="$user.userName" class="form-control" placeholder="Username" required/>
					</div>
					<div>
						<input type="email" name="primaryEmailId" value="$user.userPrimaryEmail" class="form-control" placeholder="Email" required/>
					</div>
					<div>
						<input type="password" name="password" value="$user.password" class="form-control" placeholder="Password" required/>
					</div>
					<div>
						<input type="submit" class="btn btn-default" value="Create My Account" />
					</div>

					<div class="clearfix"></div>

					<div class="separator">
						<p class="change_link">
							Already a member ? <a href="${loginUrl}" class="to_register"> Log
								in </a>
						</p>

						<div class="clearfix"></div>
						<br />

						<div>
							<h1>
								<span class="glyphicon glyphicon-education" aria-hidden="true">Welcome
								to the club!</span>
							</h1>
							<p>�MeetK 2016 All Rights Reserved.</p>
						</div>
					</div>
				</form:form>
				</section>
			</div>
		</div>
	</div>
</body>
</html>