<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<select id="${requestScope.dropdownMap[param.dropdown].id}">
		<c:forEach var="option" items="${requestScope.dropdownMap[param.dropdown].options}">
    		<option value="${option.id}" <c:if test="${option.selected}"> selected </c:if> >
        		<c:out value="${option.value}"/>
    		</option>
		</c:forEach>
	</select>
</div>