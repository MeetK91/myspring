<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<table id="${requestScope.tableMap[param.table].id}">
		<c:forEach var="columnHeader" items="${requestScope.tableMap[param.table].rows[0]}">
			<th>
				<c:out value="${columnHeader.data}"/>
			</th>
		</c:forEach>
		<c:forEach var="rows" items="${requestScope.tableMap[param.table].rows}">
			<tr>
				<c:forEach var="cell" items="${rows}">
					<td>
						<c:out value="${cell.data}"/>
					</td>
				</c:forEach>
			</tr>
		</c:forEach>
	</table>
</div>
<!--  
	<select id="${requestScope.dropdownMap[param.dropdown].id}">
		<c:forEach var="option" items="${requestScope.dropdownMap[param.dropdown].options}">
    		<option value="${option.id}" <c:if test="${option.selected}"> selected </c:if> >
        		<c:out value="${option.value}"/>
    		</option>
		</c:forEach>
	</select>
-->