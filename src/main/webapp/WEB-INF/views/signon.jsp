<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Controller URIs -->
<c:url value="/login" var="loginUrl" />
<c:url value="/signon" var="signOnUrl" />
<c:url value="/static/images" var="images"/>

<!-- Bootstrap -->
<link href="<c:url value="/static/vendors/bootstrap/dist/css/bootstrap.min.css"/>" rel="stylesheet">
<link href="<c:url value="/static/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css"/>" rel="stylesheet">

<!-- Font Awesome -->
<link href="<c:url value="/static/vendors/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet">

<!-- NProgress -->
<link href="<c:url value="/static/vendors/nprogress/nprogress.css"/>" rel="stylesheet">

<!-- Animate.css -->
<link href="<c:url value="/static/vendors/animate.css/animate.min.css"/>" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<c:url value="/static/build/css/custom.min.css"/>" rel="stylesheet">

<!-- iCheck CSS -->
<link href="<c:url value="/static/vendors/iCheck/skins/flat/green.css"/>" rel="stylesheet">

<title><spring:message code="label.signon"/></title>

</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
				<form:form class="login-form" method="post" modelAttribute="signOnDto" action="${signOnUrl}">
					<h1><spring:message code="label.signon"/></h1>
					
					<div>
						<input type="text" name="firstName" value="${firstName}" class="form-control" placeholder="<spring:message code="placeholder.user.firstname"/>"/>
						<form:errors path="firstName" cssClass="alert alert-danger" element="div"/>
					</div>
					<div>
						<input type="text" name="lastName" value="${signOnDto.lastName}" class="form-control" placeholder="<spring:message code="placeholder.user.lastname"/>"/>
						<form:errors path="lastName" cssClass="alert alert-danger" element="div"/>
					</div>
					<div>
						<input type="text" name="signUpEmailId" value="${signOnDto.signUpEmailId}" class="form-control" placeholder="<spring:message code="placeholder.user.email" />"/>
						<form:errors path="signUpEmailId" cssClass="alert alert-danger" element="div"/>
					</div>
					<div>
						<input type="password" name="signUpPassword" value="${signOnDto.signUpPassword}" class="form-control" placeholder="<spring:message code="placeholder.user.password" />"/>
						<form:errors path="signUpPassword" cssClass="alert alert-danger" element="div"/>
					</div>
					<!--  <div class="progress">
                        <div class="progress-bar progress-bar-info" data-transitiongoal="45"></div>
                    </div>-->
					<div>
						<input type="password" name="signUpConfirmPassword" value="${signOnDto.signUpConfirmPassword}" class="form-control" placeholder="<spring:message code="placeholder.user.confirmpassword" />"/>
						<div class="alert alert-info"><spring:message code="message.password.strong" /></div>
						<form:errors path="signUpConfirmPassword" cssClass="alert alert-danger" element="div"/>
					</div>
					<div>
						<input type="submit" class="btn btn-default" value="<spring:message code="label.signon"/>" />
					</div>
					
					<div class="clearfix">
					</div>

					<div class="separator">
						<p class="change_link">
							<spring:message code="label.already.member"/> <a href="${loginUrl}" class="to_register"><spring:message code="label.login"/></a>
						</p>

						<div class="clearfix"></div>
						<br />

						<div>
							<h1>
								<span class="glyphicon glyphicon-education" aria-hidden="true"></span>
								<spring:message code="message.slogan.login"/>
							</h1>
							<p><spring:message code="label.copyright"/></p>
							<%@include file="license.jsp"%>
						</div>
					</div>
				</form:form>
				</section>
			</div>
		</div>
	</div>
	
	<script src="<c:url value="/static/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"/>"></script>
	<script src="<c:url value="/static/vendors/jquery/dist/jquery.min.js"/>"></script>
	<script src="<c:url value="/static/vendors/nprogress/nprogress.js"/>"></script>
	<script src="<c:url value="/static/vendors/bootstrap/dist/js/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/static/vendors/iCheck/icheck.min.js"/>"></script>
	<script src="<c:url value="/static/vendors/fastclick/lib/fastclick.js"/>"></script>
	<script src="<c:url value="/static/build/js/custom.min.js"/>"></script>
	
		
</body>
</html>