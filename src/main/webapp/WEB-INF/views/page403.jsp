<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Access Denied!</title>

<!-- Controller URIs -->
<c:url value="/login" var="loginUrl" />
<c:url value="/signon" var="signOnUrl" />
<c:url value="/forgot" var="forgotUrl" />
<c:url value="/static/images" var="images"/>

<!-- I18n -->
<spring:message code="error.failed.login" var="i18nLoginFailed" />
<spring:message code="placeholder.user.email" var="i18nPlaceholderEmail" />
<spring:message code="message.success.logout" var="i18nLogoutSuccess" />
<spring:message code="placeholder.user.password"
	var="i18nPlaceholderPassword" />
<spring:message code="label.rememberme" var="i18nRememberMe" />
<spring:message code="label.login" var="i18nLogin" />
<spring:message code="message.newuser.login" var="i18nNewUser" />
<spring:message code="label.signon" var="i18nSignOn" />
<spring:message code="label.lost.password" var="i18nLostPassword" />
<spring:message code="label.copyright" var="i18nCopyright" />
<spring:message code="message.slogan.login" var="i18nLoginGreeting" />

<!--  Style URLs -->
<c:url value="/static/vendors/font-awesome/css/font-awesome.min.css"
	var="fontAwesomeCss" />
<c:url value="/static/vendors/bootstrap/dist/css/bootstrap.min.css"
	var="bootstrapCss" />
<c:url value="/static/vendors/animate.css/animate.min.css"
	var="animateCss" />
<c:url value="/static/vendors/nprogress/nprogress.css"
	var="nProgressCss" />
<c:url value="/static/build/css/custom.min.css" var="customThemeCss" />

<!-- Bootstrap -->
<link href="${bootstrapCss}" rel="stylesheet">

<!-- Font Awesome -->
<link href="${fontAwesomeCss}" rel="stylesheet">

<!-- NProgress -->
<link href="${nProgressCss}" rel="stylesheet">

<!-- Animate.css -->
<link href="${animateCss}" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="${customThemeCss}" rel="stylesheet">

<!-- Javascripts -->
<c:url value="/static/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js" var="bootstrapProgressJS"/>
<c:url value="/static/vendors/jquery/dist/jquery.min.js" var="jQueryMinJS"/>
<c:url value="/static/vendors/nprogress/nprogress.js" var="nProgressJS"/>
<c:url value="/static/vendors/bootstrap/dist/js/bootstrap.min.js" var="bootstrapMinJS"/>
<c:url value="/static/vendors/iCheck/icheck.min.js" var="iCheckJS"/>
<c:url value="/static/vendors/fastclick/lib/fastclick.js" var="fastClickJS"/>
<c:url value="/static/build/js/custom.min.js" var="customThemeJS"/>

</head>

<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<!-- page content -->
			<div class="col-md-12">
				<div class="col-middle">
					<div class="text-center text-center">
						<h1 class="error-number">403</h1>
						<h2>Access denied</h2>
						<p>
							You are not authorized to access this resource. <a href="#">Report this?</a>
						</p>
					</div>
					<div class="text-center text-center">
						<%@include file="license.jsp"%>
					</div>
				</div>
			</div>
			<!-- /page content -->
		</div>
	</div>

	<script src="${bootstrapProgressJS}"></script>
	<script src="${jQueryMinJS}"></script>
	<script src="${nProgressJS}"></script>
	<script src="${bootstrapMinJS}"></script>
	<script src="${iCheckJS}"></script>
	<script src="${fastClickJS}"></script>
	<script src="${customThemeJS}"></script>
</body>
</html>