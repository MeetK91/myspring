<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>

<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Controller URIs -->
<c:url value="/login" var="loginUrl" />
<c:url value="/signon" var="signOnUrl" />
<c:url value="/forgot" var="forgotUrl"/>
<c:url value="/static/images" var="images"/>

<!-- Bootstrap -->
<link href="<c:url value="/static/vendors/bootstrap/dist/css/bootstrap.min.css"/>" rel="stylesheet">

<!-- Font Awesome -->
<link href="<c:url value="/static/vendors/font-awesome/css/font-awesome.min.css"/>" rel="stylesheet">

<!-- NProgress -->
<link href="<c:url value="/static/vendors/nprogress/nprogress.css"/>" rel="stylesheet">

<!-- Animate.css -->
<link href="<c:url value="/static/vendors/animate.css/animate.min.css"/>" rel="stylesheet">

<!-- Custom Theme Style -->
<link href="<c:url value="/static/build/css/custom.min.css"/>" rel="stylesheet">

<title><spring:message code="label.login"/></title>

</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signin"></a>
		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
				<form:form class="login-form" method="post" action="${loginUrl}">
					<h1><spring:message code="label.login"/></h1>
					<c:if test="${param.error != null}">
						<div class="alert alert-danger">
							<p><spring:message code="error.failed.login"/></p>
						</div>
					</c:if>
					<c:if test="${param.logout != null}">
						<div class="alert alert-success">
							<p><spring:message code="message.success.logout"/></p>
						</div>
					</c:if>
					<c:if test="${param.signon != null}">
						<div class="alert alert-success">
							<p><spring:message code="message.success.signon"/></p>
						</div>
					</c:if>
					<div class="checkbox">
						<input type="email" name="loginEmailId" class="form-control" placeholder="<spring:message code="placeholder.user.email"/>" required autofocus/>
						<form:errors path="loginEmailId" cssClass="alert alert-danger" element="div"/>
					</div>
					<div>
						<input type="password" name="password" class="form-control"  placeholder="<spring:message code="placeholder.user.password"/>" required/>
					</div>
					<div class="checkbox">
						<label>
						<input type="checkbox" id="rememberme" name="remember-me"/><spring:message code="label.rememberme"/>
						</label>
					</div>
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<div>
						<input type="submit" class="btn btn-default submit" value="<spring:message code="label.login"/>" />
						<a class="reset_pass" href="${forgotUrl}"><spring:message code="label.lost.password"/></a>
					</div>

					<div class="clearfix"></div>

					<div class="separator">
						<p class="change_link">
							<spring:message code="message.newuser.login"/> <a href="${signOnUrl}" class="to_register"><b><spring:message code="label.signon"/></b></a>
						</p>
						<div>
							<h1>
								<span class="glyphicon glyphicon-education" aria-hidden="true">
								<spring:message code="message.slogan.login"/>
								</span>
							</h1>
							<p><spring:message code="message.copyright"/></p>
							<%@include file="license.jsp"%>
						</div>
					</div>
				</form:form>
				</section>
			</div>
		</div>
	</div>
</body>
</html>